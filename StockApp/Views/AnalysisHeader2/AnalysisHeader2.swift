//
//  AnalysisHeader2.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

class AnalysisHeader2: UIView {

    @IBOutlet weak var periodSegment: UISegmentedControl!
    
    @IBAction func periodChange(_ sender: Any) {
        
        let periodNumber = periodSegment.selectedSegmentIndex
        
        if let topView = UIApplication.topViewController() as? AnalysisController{
            
            topView.periodNumber = periodNumber + 1
            
            UIView.setAnimationsEnabled(false)
            topView.tableView.beginUpdates()
            topView.tableView.reloadSections(NSIndexSet(index: 1) as IndexSet, with: UITableViewRowAnimation.none)
            topView.tableView.endUpdates()
            UIView.setAnimationsEnabled(true)
            
        }
        
    }
    
    public func updateSegments(number: Int){
        
        if periodSegment.numberOfSegments < number{
            
            for i in periodSegment.numberOfSegments ..< number{
                periodSegment.insertSegment(withTitle: "T\(i+1)", at: periodSegment.numberOfSegments, animated: true)
                
            }
            
        } else if periodSegment.numberOfSegments > number{
            
            if periodSegment.selectedSegmentIndex > number-1{
                periodSegment.selectedSegmentIndex = number-1
                periodChange(periodSegment)
                
            }
            
            for _ in number ..< periodSegment.numberOfSegments{
                periodSegment.removeSegment(at: periodSegment.numberOfSegments-1, animated: true)
                
            }
            
        }
        
    }
    
}
