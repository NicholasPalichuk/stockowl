//
//  SearchCell.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak fileprivate var tickerLabel: UILabel!
    @IBOutlet weak fileprivate var exchangeLabel: UILabel!
    @IBOutlet weak fileprivate var nameLabel: UILabel!
    
    @IBOutlet weak fileprivate var priceLabel: UILabel!
    @IBOutlet weak fileprivate var currencyLabel: UILabel!
    @IBOutlet weak fileprivate var marketcapLabel: UILabel!
    
}

extension SearchCell{
    
    public func setLabels(ticker: String?,exchange: String?,name: String?,price: String?,currency: String?,marketcap: String?){
        
        tickerLabel.text = ticker
        exchangeLabel.text = exchange
        nameLabel.text = name
        priceLabel.text = price
        currencyLabel.text = currency
        marketcapLabel.text = "Mkt Cap: \(marketcap!)"
        
    }
    
    public func setRecentColors(){
        
        tickerLabel.textColor = UIColor.gray
        priceLabel.textColor = UIColor.gray
        
    }
    
    public func setColors(){
        
        tickerLabel.textColor = UIColor.black
        priceLabel.textColor = UIColor.black
        
    }
    
}













