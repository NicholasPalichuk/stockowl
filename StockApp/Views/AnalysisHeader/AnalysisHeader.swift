//
//  AnalysisHeader.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

class AnalysisHeader: UIView {

    var statementValue = 0
    var timeFrameValue = 0
    
    var columnCount = 0
    
    @IBOutlet weak var statementSegmented: UISegmentedControl!
    @IBOutlet weak var timeFrameSegmented: UISegmentedControl!
    
    @IBAction func statementChange(_ sender: Any) {
        
        statementValue = statementSegmented.selectedSegmentIndex
        self.updateAnalysis()
        
    }
    
    @IBAction func timeFrameChange(_ sender: Any) {
        
        timeFrameValue = timeFrameSegmented.selectedSegmentIndex
        self.updateAnalysis()
        
    }
    
    func updateAnalysis(){
        
        if let topView = UIApplication.topViewController() as? AnalysisController{
            
            topView.graphNumber = 2*statementValue + timeFrameValue
            topView.tableView.reloadData()
            
            if topView.numberOfColumns != columnCount{
                
                columnCount = topView.numberOfColumns
                topView.headerView2?.updateSegments(number: columnCount-1)
                
            }
            
            UIView.setAnimationsEnabled(false)
            topView.tableView.beginUpdates()
            topView.tableView.reloadSections(NSIndexSet(index: 1) as IndexSet, with: UITableViewRowAnimation.none)
            topView.tableView.endUpdates()
            UIView.setAnimationsEnabled(true)
            
        }
        
    }
    
}



















