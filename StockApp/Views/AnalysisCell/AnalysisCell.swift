//
//  AnalysisCell.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

class AnalysisCell: UITableViewCell {
    
    @IBOutlet weak fileprivate var entryLabel: UILabel!
    
    @IBOutlet weak fileprivate var valueLabel: UILabel!
    @IBOutlet weak fileprivate var stackView: UIStackView!
    
    @IBOutlet weak var seperatorHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomStackViewMargin: NSLayoutConstraint!
    
}

//MARK: Set
extension AnalysisCell{
    
    func setLabels(entry:String,value:String){
        
        entryLabel.text = entry
        valueLabel.text = value
        
        var doubleValue = value
        
        doubleValue = value.replacingOccurrences(of: ",", with: "", options: .literal, range: nil)
        
        if let double = Double(doubleValue) {
            if double < 0{
                valueLabel.textColor = UIColor.red
            }else{
                valueLabel.textColor = UIColor.black
            }
        }else{
            valueLabel.textColor = UIColor.black
        }
        
    }
    
}

//MARK: Title
extension AnalysisCell{
    
    public func setTitle(){
        
        removeConstraintOfValue()
        setStackViewCenterAlignment()
        setBackgroundGrey()
        
    }
    
//    fileprivate func setStackViewTopAlignment(){
//        stackView.alignment = .top
//
//    }
    
    fileprivate func setBackgroundGrey(){
        contentView.backgroundColor = UIColor(hexadecimal: 0xCBCBCB)
        
    }
    
    fileprivate func removeConstraintOfValue(){
        
        valueLabel.removeConstraints(valueLabel.constraints)
        
    }
    
}

//MARK: Body
extension AnalysisCell{
    
    public func setBody(row: Int,graph: Int){
        
        setBiggerSeperator(row: row, graph: graph)
        setConstraintOfValue()
        setStackViewCenterAlignment()
        setBackgroundWhite()
        
    }
    
    fileprivate func setBiggerSeperator(row: Int,graph: Int){
        
        switch graph {
            
        case 0:
            for biggerRows in rowsWithBiggerSeperatorsTable0{
                
                if row == biggerRows{
                    
                    seperatorHeight.constant = 5
                    bottomStackViewMargin.constant = 7
                    break
                    
                }else{
                    
                    seperatorHeight.constant = 1
                    bottomStackViewMargin.constant = 2
                    
                }
                
            }
        case 1:
            for biggerRows in rowsWithBiggerSeperatorsTable1{
                
                if row == biggerRows{
                    
                    seperatorHeight.constant = 5
                    bottomStackViewMargin.constant = 7
                    break
                    
                }else{
                    
                    seperatorHeight.constant = 1
                    bottomStackViewMargin.constant = 2
                    
                }
                
            }
        case 2:
            for biggerRows in rowsWithBiggerSeperatorsTable2{
                
                if row == biggerRows{
                    
                    seperatorHeight.constant = 5
                    bottomStackViewMargin.constant = 7
                    break
                    
                }else{
                    
                    seperatorHeight.constant = 1
                    bottomStackViewMargin.constant = 2
                    
                }
                
            }
        case 3:
            for biggerRows in rowsWithBiggerSeperatorsTable3{
                
                if row == biggerRows{
                    
                    seperatorHeight.constant = 5
                    bottomStackViewMargin.constant = 7
                    break
                    
                }else{
                    
                    seperatorHeight.constant = 1
                    bottomStackViewMargin.constant = 2
                    
                }
                
            }
        case 4:
            for biggerRows in rowsWithBiggerSeperatorsTable4{
                
                if row == biggerRows{
                    
                    seperatorHeight.constant = 5
                    bottomStackViewMargin.constant = 7
                    break
                    
                }else{
                    
                    seperatorHeight.constant = 1
                    bottomStackViewMargin.constant = 2
                    
                }
                
            }
        case 5:
            for biggerRows in rowsWithBiggerSeperatorsTable5{
                
                if row == biggerRows{
                    
                    seperatorHeight.constant = 5
                    bottomStackViewMargin.constant = 7
                    break
                    
                }else{
                    
                    seperatorHeight.constant = 1
                    bottomStackViewMargin.constant = 2
                    
                }
                
            }
            
        default:
            break
            
        }
        
    }
    
    fileprivate func setStackViewCenterAlignment(){
        stackView.alignment = .center
        
    }
    
    
    fileprivate func setBackgroundWhite(){
        contentView.backgroundColor = UIColor.white
        
    }
    
    fileprivate func setConstraintOfValue(){
        
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        let widthConstraint = valueLabel.widthAnchor.constraint(equalToConstant: 90)
        valueLabel.addConstraint(widthConstraint)
        
    }
    
}














