//
//  UIColor.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

extension UIColor{
    
    convenience public init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience public init(hexadecimal: Int) {
        self.init(red:(hexadecimal >> 16) & 0xff, green:(hexadecimal >> 8) & 0xff, blue:hexadecimal & 0xff)
    }
    
}
