//
//  Array.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-31.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import Foundation

extension Array {
    
    func filterDuplicates( includeElement: @escaping (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
        var results = [Element]()
        
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        
        return results
    }
}
