//
//  UIScreen.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-31.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

extension UIScreen{
    public static var width: CGFloat{
        return main.bounds.width
    }
    
    public static var height: CGFloat{
        return main.bounds.height
    }
}
