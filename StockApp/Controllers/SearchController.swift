//
//  SearchController.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import StoreKit
import Firebase

class SearchController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate{

    var data = [Stock]()
    
    var recentStockSearch = [SearchResultStock]()
    
    var recentStocks = [Stock]()
    
    var showRecents = true
    
    var stillSearch = true
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Auth.auth().signInAnonymously { (user, error) in}
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.setupTable()
        self.setupRecent()
        
        searchBar.delegate = self
        
        searchBar.placeholder = "Search"
        self.navigationItem.titleView = searchBar
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let leftReview = UserDefaults.standard.object(forKey: "leftReview") as? Bool{
            
            if leftReview{
                return
            }
            
        }
        
        let currentDate = NSDate()
        
        if let lastReviewDate = UserDefaults.standard.object(forKey: "lastReviewDate") as? NSDate{
            
            if currentDate.timeIntervalSince1970 > lastReviewDate.timeIntervalSince1970 + 800000{
                
                presentReviewProcess(currentDate: currentDate)
                
            }
            
        }else{
            
            UserDefaults.standard.set(currentDate, forKey: "lastReviewDate")
            
        }
        
    }
    
    func presentReviewProcess(currentDate:NSDate){
        
        let aC = UIAlertController(title: "Feedback", message: "\nHas StockOwl been useful so far? \n\nAny and all feedback will help make StockOwl better \n\n😃", preferredStyle: UIAlertControllerStyle.alert)
        
        aC.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alertAction) in
            
            SKStoreReviewController.requestReview()
            UserDefaults.standard.set(true, forKey: "leftReview")
            
        }))
        
        aC.addAction(UIAlertAction(title: "No", style: .default, handler: { (alertAction) in
            
            let alert = UIAlertController(title: "Feedback", message: "\n\nLet us know how we can improve StockOwl for you.", preferredStyle: .alert)
            
            alert.addTextField { (textField) in}
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                
                var ref: DatabaseReference!
                ref = Database.database().reference()
                
                ref.child("Reviews").child((Auth.auth().currentUser?.uid)!).setValue(["Review": textField?.text])

                let received = UIAlertController(title: "Thank you", message: "Your feedback has been received", preferredStyle: .alert)
                received.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                
                self.present(received, animated: false, completion: nil)
                
            }))
            
            self.present(alert, animated: false, completion: nil)
            
        }))
        
        UserDefaults.standard.set(currentDate, forKey: "lastReviewDate")
        
        present(aC, animated: true) {}
        
    }
    
}

extension SearchController{
    
    @objc func keyboardWillShow(notification:NSNotification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom:keyboardHeight, right: 0)
            
        }
        
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0)
        
    }
    
}

extension SearchController{
    
    fileprivate func setupRecent(){
        
        if let recentStockSearchCount = UserDefaults.standard.object(forKey: "RECENTSTOCKSSEARCH_COUNT") as? Int{
            
            for i in 0..<recentStockSearchCount{
                
                if let dict = UserDefaults.standard.object(forKey: "RECENTSTOCKSSEARCH\(i)") as? [String:String]{
                    
                    let recentSearch = SearchResultStock()
                    recentSearch.name = dict["name"]
                    recentSearch.symbol = dict["symbol"]
                    recentSearch.exchDisp = dict["exchDisp"]
                    
                    recentStockSearch.append(recentSearch)
                    
                }
                
            }
            
        }
        
        if let recentStockCount = UserDefaults.standard.object(forKey: "RECENTSTOCKS_COUNT") as? Int{
            
            for i in 0..<recentStockCount{
                
                if let dict = UserDefaults.standard.object(forKey: "RECENTSTOCKS\(i)") as? [String:String]{
                    
                    let recentStock = Stock()
                    recentStock.name = dict["name"]
                    recentStock.currency = dict["currency"]
                    recentStock.exchange = dict["exchange"]
                    recentStock.marketCap = dict["marketCap"]
                    recentStock.price = dict["price"]
                    recentStock.ticker = dict["ticker"]
                    
                    recentStocks.append(recentStock)
                    
                }
                
            }
            
        }
        
        if recentStocks.count == 0{
            
            for i in 1...5 {
                
                let recentSearch = SearchResultStock()
                
                if i == 1{
                    
                    recentSearch.name = "Apple Inc."
                    recentSearch.symbol = "AAPL"
                    recentSearch.exchDisp = "NASDAQ"
                    
                } else if i == 2{
                    
                    recentSearch.name = "Alphabet Inc."
                    recentSearch.symbol = "GOOG"
                    recentSearch.exchDisp = "NASDAQ"
                    
                } else if i == 3{
                    
                    recentSearch.name = "Amazon.com, Inc."
                    recentSearch.symbol = "AMZN"
                    recentSearch.exchDisp = "NASDAQ"
                    
                } else if i == 4{
                    
                    recentSearch.name = "Microsoft Corporation"
                    recentSearch.symbol = "MSFT"
                    recentSearch.exchDisp = "NASDAQ"
                    
                } else if i == 5{
                    
                    recentSearch.name = "Facebook, Inc."
                    recentSearch.symbol = "FB"
                    recentSearch.exchDisp = "NASDAQ"
                    
                }
                
                recentStockSearch.append(recentSearch)
                
            }
            
            var stockCount = 0
            let endCount = recentStockSearch.count
            
            for stockSearch in recentStockSearch{
                
                var str = stockSearch.symbol!
                
                if let dotRange = str.range(of: ".") {
                    str.removeSubrange(dotRange.lowerBound..<str.endIndex)
                    stockSearch.symbol = str
                }
                
                if let dashRange = stockSearch.symbol!.range(of: "-") {
                    stockSearch.symbol!.replaceSubrange(dashRange, with: ".")
                }
                
                if stockSearch.exchDisp == "Toronto"{
                    stockSearch.exchDisp = "TSE"
                    
                }else if stockSearch.exchDisp == "CDNX"{
                    stockSearch.exchDisp = "CVE"
                    
                }
                
                self.getStockDataFrom(stockSearch: stockSearch, completionHandler: { (stock) in
                    
                    stockCount += 1
                    
                    if stock != nil{
                        
                        self.recentStocks.append(stock!)
                        
                        if stockCount == endCount{
                            if self.stillSearch {
                                self.doneSearchRecent()
                                
                            }
                            
                        }
                        
                    }
                    
                    if stockCount == endCount{
                        if self.stillSearch {
                            self.doneSearchRecent()
                            
                        }
                        
                    }
                    
                })
                
            }
            
            saveRecentStocks()
            
        }
        
    }
    
    fileprivate func setupTable(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(SearchCell.nib(), forCellReuseIdentifier: SearchCell.cellReuseIdentifier())
        
    }
    
}

//MARK: UITableView Delegate
extension SearchController{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if data.count == 0 && searchBar.text?.characters.count == 0{
            
            showRecents = true
            return recentStocks.count
            
        }
        
        showRecents = false
        return data.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: SearchCell?
        cell = tableView.dequeueReusableCell(withIdentifier: SearchCell.cellReuseIdentifier()) as! SearchCell?
        
        if showRecents{
            
            let row = tableView.numberOfRows(inSection: 0) - 1 - indexPath.row
            
            if recentStocks.count > row{
                
                let ticker = recentStocks[row].ticker
                let name = recentStocks[row].name
                let price = recentStocks[row].price
                let currency = recentStocks[row].currency
                let marketcap = recentStocks[row].marketCap
                let exchange = recentStocks[row].exchange
                
                let priceAsString = "\(price!)"
                
                cell?.setLabels(ticker: ticker, exchange: exchange, name: name, price: priceAsString, currency: currency, marketcap: marketcap)
                cell?.setRecentColors()
                
            }
            
        }else{
            
            if data.count > indexPath.row{
                
                let ticker = data[indexPath.row].ticker
                let name = data[indexPath.row].name
                let price = data[indexPath.row].price
                let currency = data[indexPath.row].currency
                let marketcap = data[indexPath.row].marketCap
                let exchange = data[indexPath.row].exchange
                
                let priceAsString = "\(price!)"
                
                cell?.setLabels(ticker: ticker, exchange: exchange, name: name, price: priceAsString, currency: currency, marketcap: marketcap)
                cell?.setColors()
                
            }
            
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if showRecents{
            
            let row = tableView.numberOfRows(inSection: 0) - 1 - indexPath.row
            
            if recentStocks.count > row{
                
                let name = recentStocks[row].name!
                let ticker = recentStocks[row].ticker!
                let exchange = recentStocks[row].exchange!
                
//                let stockSearch = SearchResultStock()
//                
//                stockSearch.exchDisp = exchange
//                stockSearch.name = name
//                stockSearch.symbol = ticker
//                
//                addToRecent(stock: recentStocks[row], stockSearch: stockSearch)
                
                GLOBAL_NAME = name
                GLOBAL_TICKER = ticker
                GLOBAL_EXCHANGE = exchange
                
                searchBar.resignFirstResponder()
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let analysisController = storyBoard.instantiateViewController(withIdentifier: "AnalysisController") as! AnalysisController
                self.navigationController?.pushViewController(analysisController, animated: true)
                
            }
            
        }else{
            
            if data.count > indexPath.row{
                
                let name = data[indexPath.row].name!
                let ticker = data[indexPath.row].ticker!
                let exchange = data[indexPath.row].exchange!
                
                let stockSearch = SearchResultStock()
                
                stockSearch.exchDisp = exchange
                stockSearch.name = name
                stockSearch.symbol = ticker
                
                addToRecent(stock: data[indexPath.row], stockSearch: stockSearch)
                
                GLOBAL_NAME = name
                GLOBAL_TICKER = ticker
                GLOBAL_EXCHANGE = exchange
                
                searchBar.resignFirstResponder()
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let analysisController = storyBoard.instantiateViewController(withIdentifier: "AnalysisController") as! AnalysisController
                self.navigationController?.pushViewController(analysisController, animated: true)
                
            }
            
        }
        
    }
    
}

extension SearchController{
    
    fileprivate func addToRecent(stock: Stock, stockSearch: SearchResultStock){
        
        var notAlreadyRecent = true
        
        var alreadyCount = 0
        
        for recentStock in recentStocks{
            
            alreadyCount += 1
            
            print(recentStock.name)
            print(stock.name)
            
            if recentStock.name == stock.name{
                notAlreadyRecent = false
                break
                
            }
            
        }
        
        if notAlreadyRecent{
            
            if recentStocks.count >= RECENT_STOCK_LIMIT {
                
                for i in 0..<recentStocks.count-1{
                    
                    recentStockSearch[i] = recentStockSearch[i+1]
                    recentStocks[i] = recentStocks[i+1]
                    
                }
                
                recentStockSearch[recentStockSearch.count-1] = stockSearch
                recentStocks[recentStocks.count-1] = stock
                
            }else{
                
                recentStockSearch.append(stockSearch)
                recentStocks.append(stock)
                
            }
            
            self.saveRecentStocks()
            
        }
//        else{
//
//            for i in 0..<recentStocks.count-alreadyCount{
//
//                recentStockSearch[i] = recentStockSearch[i+1]
//                recentStocks[i] = recentStocks[i+1]
//
//            }
//
//            recentStockSearch[recentStockSearch.count-1] = stockSearch
//            recentStocks[recentStocks.count-1] = stock
//
//        }
        
    }
    
    fileprivate func saveRecentStocks(){
    
        UserDefaults.standard.set(recentStockSearch.count, forKey: "RECENTSTOCKSSEARCH_COUNT")
        
        for i in 0..<recentStockSearch.count{
            
            let recentStock = recentStockSearch[i]
            
            let recentStockDict = ["name": recentStock.name,
                                   "symbol": recentStock.symbol,
                                   "exchDisp": recentStock.exchDisp]
            
            UserDefaults.standard.set(recentStockDict, forKey: "RECENTSTOCKSSEARCH\(i)")
            
        }
        
        UserDefaults.standard.set(recentStocks.count, forKey: "RECENTSTOCKS_COUNT")
        
        for i in 0..<recentStocks.count{
            
            let recentStock = recentStocks[i]
            
            let recentStockDict = ["price": recentStock.price,
                                   "name": recentStock.name,
                                   "ticker": recentStock.ticker,
                                   "currency": recentStock.currency,
                                   "exchange": recentStock.exchange,
                                   "marketCap": recentStock.marketCap]
            
            UserDefaults.standard.set(recentStockDict, forKey: "RECENTSTOCKS\(i)")
            
        }
    
    }
    
    fileprivate func doneSearch(){
        
        if data.count == 0{
            
            navigationItem.prompt = "Not Found"
            
        }else{
            
            navigationItem.prompt = "Results"
            
        }
        
        data = self.data.filterDuplicates { $0.name == $1.name}
        data.sort {$0.marketCap! > $1.marketCap!}
        self.tableView.reloadData()
        
    }
    
    fileprivate func doneSearchRecent(){
        
        if recentStocks.count == 0{
            
            navigationItem.prompt = "No Recents"
            
        }else{
            
            navigationItem.prompt = "Recently Visited"
            
        }

        self.tableView.reloadData()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != ""{
            
            self.data.removeAll()
            self.stillSearch = true
            
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                if searchText.characters.count == searchBar.text?.characters.count{
                    
                    self.navigationItem.prompt = "Searching..."
                    
                    _ = BigBoard.stocksContainingSearchTerm(searchTerm: searchText, success: { (searchResult) in
                        
                        var stockCount = 0
                        let endCount = searchResult.count
                        
                        var stocksFound = [Stock]()
                        
                        if searchResult.count == 0{
                            if self.stillSearch {
                                self.data.removeAll()
                                self.doneSearch()
                                
                            }
                            return
                        }
                        
                        for stockSearch in searchResult{
                            
                            if (stockSearch.exchDisp == "NASDAQ" || stockSearch.exchDisp == "NYSE" || stockSearch.exchDisp == "Toronto" || stockSearch.exchDisp == "CDNX" || stockSearch.exchDisp == "NYSE MKT"){
                                
                                var str = stockSearch.symbol!
                                
                                if let dotRange = str.range(of: ".") {
                                    str.removeSubrange(dotRange.lowerBound..<str.endIndex)
                                    stockSearch.symbol = str
                                }
                                
                                if let dashRange = stockSearch.symbol!.range(of: "-") {
                                    stockSearch.symbol!.replaceSubrange(dashRange, with: ".")
                                }
                                
                                if stockSearch.exchDisp == "Toronto"{
                                    stockSearch.exchDisp = "TSE"
                                    
                                }else if stockSearch.exchDisp == "CDNX"{
                                    stockSearch.exchDisp = "CVE"
                                    
                                }else if stockSearch.exchDisp == "NYSE MKT"{
                                    stockSearch.exchDisp = "NYSEAMERICAN"
                                    
                                }
                                
                                self.getStockDataFor(stockSearch: stockSearch, completionHandler: { (stock) in
                                    
                                    stockCount += 1
                                    
                                    if stock != nil{
                                        
                                        let containsStock = stocksFound.contains(where: { (stockResponse) -> Bool in
                                            if stockResponse.name == stock?.name{
                                                return stockResponse.exchange == stock?.exchange
                                                
                                            }
                                            
                                            return false
                                            
                                        })
                                        
                                        if !containsStock {
                                            
                                            if stock?.currency != nil && stock?.price != nil && stock?.name != nil && stock?.ticker != nil && stock?.exchange != nil && stock?.marketCap != nil{
                                                
                                                if self.stillSearch{
                                                    
                                                    stocksFound.append(stock!)
                                                    self.data.append(stock!)
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                        if stockCount == endCount{
                                            if self.stillSearch {
                                                self.doneSearch()
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if stockCount == endCount{
                                        if self.stillSearch {
                                            self.doneSearch()
                                            
                                        }
                                        
                                    }
                                    
                                })
                                
                            }else{
                                
                                stockCount += 1
                                
                                if stockCount == endCount{
                                    if self.stillSearch {
                                        self.doneSearch()
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }) { (error) in
                        print(error)
                        
                    }
                    
                }
                
            }
            
        }else{
            
            self.data.removeAll()
            self.stillSearch = false
            self.navigationItem.prompt = "Recently Visited"
            self.tableView.reloadData()
            
        }
        
    }
    
}








