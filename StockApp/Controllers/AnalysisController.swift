//
//  AnalysisController.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-22.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import Kanna
import Alamofire

class AnalysisController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK: Variables
    
    var graphNumber = 0
    var numberOfColumns = 0
    var periodNumber = 1
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var tableArray = [[String]]()
    
    let headerView = UIView.loadFromNibWithName(nibNamed: "AnalysisHeader") as? AnalysisHeader
    let headerView2 = UIView.loadFromNibWithName(nibNamed: "AnalysisHeader2") as? AnalysisHeader2
    
}

//MARK: ViewController
extension AnalysisController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = GLOBAL_NAME
        
        self.setupTable()
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        self.getTableDataFor(ticker: GLOBAL_TICKER, exchange: GLOBAL_EXCHANGE) { (strArray) in
            
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
            if let tempTableArray = strArray{
                self.tableArray = tempTableArray
                self.presentData()
                
            }else{
                
                let alert = UIAlertController(title: "No Data", message: "There was no data found for this stock.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
}

//MARK Setup
extension AnalysisController{
    
    fileprivate func setupTable(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(AnalysisCell.nib(), forCellReuseIdentifier: AnalysisCell.cellReuseIdentifier())
        
    }
    
}

//MARK: Present Data
extension AnalysisController{
    
    func presentData(){
        
        if tableArray.count == 0{
            
            let alert = UIAlertController(title: "No Data", message: "There was no data found for this stock.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) in
                self.navigationController?.popToRootViewController(animated: true)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            tableView.reloadData()
            
        }
        
    }
    
}

//MARK: UITableViewDelegate
extension AnalysisController{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
        
    }
    
    func getRowNumber() -> Int{
        
        var count = 0
        
        for item in tableArray[graphNumber]{
            
            count += 1
            
            if item == "-"{
                numberOfColumns = count - 2
                return tableArray[graphNumber].count / numberOfColumns
                
            }
            
            if Double(item) != nil{
                numberOfColumns = count - 2
                return tableArray[graphNumber].count / numberOfColumns
                
            }
            
            var str = item
            
            if let commaRange = str.range(of: ",") {
                
                str.removeSubrange(commaRange.lowerBound..<str.endIndex)
                
                if Int(str) != nil{
                    numberOfColumns = count - 2
                    return tableArray[graphNumber].count / numberOfColumns
                    
                }
            }
            
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1{
            
            if tableArray.count > 0{
            
                return getRowNumber()
            
            }
            
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 50
            
        }
        
//        let entryPoint = indexPath.row*5
        let row = indexPath.row
        
        var extraRoom = 0
        
        switch graphNumber {
            
        case 0:
            for biggerRow in rowsWithBiggerSeperatorsTable0{
                
                if row == biggerRow{
                    
                    extraRoom = 4
                    break
                    
                }
                
            }
        case 1:
            for biggerRow in rowsWithBiggerSeperatorsTable1{
                
                if row == biggerRow{
                    
                    extraRoom = 4
                    break
                    
                }
                
            }
        case 2:
            for biggerRow in rowsWithBiggerSeperatorsTable2{
                
                if row == biggerRow{
                    
                    extraRoom = 4
                    break
                    
                }
                
            }
        case 3:
            for biggerRow in rowsWithBiggerSeperatorsTable3{
                
                if row == biggerRow{
                    
                    extraRoom = 4
                    break
                    
                }
                
            }
        case 4:
            for biggerRow in rowsWithBiggerSeperatorsTable4{
                
                if row == biggerRow{
                    
                    extraRoom = 4
                    break
                    
                }
                
            }
        case 5:
            for biggerRow in rowsWithBiggerSeperatorsTable5{
                
                if row == biggerRow{
                    
                    extraRoom = 4
                    break
                    
                }
                
            }
            
        default:
            break
            
        }
        
//        if tableArray[graphNumber][entryPoint].characters.count > 40{
//
//            return CGFloat(50 + extraRoom)
//
//        }
        
        return CGFloat(30 + extraRoom)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0{
            return 98
            
        }
        
        return 64
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0{
            return headerView
        }else{
            return headerView2
        }
        
    }
    
}

//MARK: UITableViewDataSource
extension AnalysisController{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: AnalysisCell?
        cell = tableView.dequeueReusableCell(withIdentifier: AnalysisCell.cellReuseIdentifier()) as! AnalysisCell?
        
        let entryPoint = indexPath.row*numberOfColumns
        
        let entryText = tableArray[graphNumber][entryPoint]
        let valueText = tableArray[graphNumber][entryPoint + periodNumber]
        
        cell?.setLabels(entry: entryText, value: valueText)
        
        if indexPath.row == 0{
            cell?.setTitle()
            
        }else{
            cell?.setBody(row: indexPath.row, graph: graphNumber)
            
        }
        
        return cell!
        
    }
    
}












