//
//  AnalysisModel.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import Kanna
import Alamofire

//MARK: Data Collection
extension AnalysisController{
    
    public func getTableDataFor(ticker: String,exchange: String,completionHandler: @escaping CompletionHandlerStringArray) -> Void {
        Alamofire.request("https://finance.google.ca/finance?q=\(exchange)%3A\(ticker)&fstype=ii&ei=pGn2WYihAoLa2Aa0h6vwBQ").responseString { response in
            if let html = response.result.value {
                if let table = self.getFinancialInformation(html: html){
                    completionHandler(table)
                    
                }else{
                    completionHandler(nil)
                    
                }
                
            }else{
                completionHandler(nil)
                
            }
        }
    }
    
    fileprivate func getFinancialInformation(html: String) -> [[String]]? {
        if let doc = Kanna.HTML(html: html, encoding: String.Encoding.utf8) {
            
            var tempArray = [String]()
            
            for show in doc.css("table[id='fs-table']") {
                
                let showString = show.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                if showString != ""{
                    tempArray.append(showString)
                    
                }
                
            }
            
            var tArray = [[String]]()
            
            for string in tempArray{
                
                var table = string.components(separatedBy: .newlines)
                table = table.filter({ (component) -> Bool in

                    if component.characters.count > 0 {
                        return true

                    }

                    return false

                })
                
                tArray.append(table)
                
            }
            
            return tArray
            
        }
        
        return nil
    }
    
}
