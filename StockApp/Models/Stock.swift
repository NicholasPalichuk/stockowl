//
//  Stock.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-11-02.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

class Stock: NSObject{
    
    open var price: String?
    open var name: String?
    open var ticker: String?
    open var exchange: String?
    open var currency: String?
    open var marketCap: String?
    
}

class SearchResultStock: NSObject{
    
    open var name: String?
    open var symbol: String?
    open var exchDisp: String?
    
}
