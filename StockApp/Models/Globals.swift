//
//  Globals.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-10-30.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit

typealias CompletionHandlerStringArray = (_ stringArray:[[String]]?) -> Void
typealias CompletionHandlerStock = (_ stringArray:Stock?) -> Void

var GLOBAL_TICKER = ""
var GLOBAL_NAME = ""
var GLOBAL_EXCHANGE = ""

var RECENT_STOCK_LIMIT = 25

let rowsWithBiggerSeperatorsTable0 = [2,4,11,12,16,17,20,24,26,27,30,33,34]
let rowsWithBiggerSeperatorsTable1 = [2,4,11,12,16,17,20,24,26,27,30,33,34]
let rowsWithBiggerSeperatorsTable2 = [9,16,22,25,26,30,38,39,41]
let rowsWithBiggerSeperatorsTable3 = [9,16,22,25,26,30,38,39,41]
let rowsWithBiggerSeperatorsTable4 = [6,9,14,16]
let rowsWithBiggerSeperatorsTable5 = [6,9,14,16]
