//
//  SearchModel.swift
//  StockApp
//
//  Created by Nicholas Palichuk on 2017-11-02.
//  Copyright © 2017 Nicholas Palichuk. All rights reserved.
//

import UIKit
import Kanna
import Alamofire

extension SearchController{
    
    public func getStockDataFor(stockSearch: BigBoardSearchResultStock,completionHandler: @escaping CompletionHandlerStock) -> Void {
        
        Alamofire.request("https://finance.google.ca/finance?q=\(stockSearch.exchDisp!):\(stockSearch.symbol!)").responseString { response in
            if let html = response.result.value {
                
                if let stock = self.getStockInformation(html: html,name: stockSearch.symbol!){
                    
                    stock.name = stockSearch.name
                    stock.ticker = stockSearch.symbol
                    
                    if stockSearch.exchDisp == "NASDAQ" || stockSearch.exchDisp == "NYSE" || stockSearch.exchDisp == "NYSEAMERICAN"{
                        stock.currency = "USD"
                        stock.exchange = stockSearch.exchDisp
                        
                    }else if stockSearch.exchDisp == "TSE"{
                        stock.currency = "CAD"
                        stock.exchange = "TSE"
                        
                    }else if stockSearch.exchDisp == "CVE"{
                        stock.currency = "CAD"
                        stock.exchange = "CVE"
                        
                    }
                    completionHandler(stock)
                    
                }else{
                    completionHandler(nil)
                    
                }
                
            }else{
                completionHandler(nil)
                
            }
        }
    }
    
    public func getStockDataFrom(stockSearch: SearchResultStock,completionHandler: @escaping CompletionHandlerStock) -> Void {
        Alamofire.request("https://finance.google.ca/finance?q=\(stockSearch.exchDisp!):\(stockSearch.symbol!)").responseString { response in
            if let html = response.result.value {
                
                if let stock = self.getStockInformation(html: html,name: stockSearch.symbol!){
                    
                    stock.name = stockSearch.name
                    stock.ticker = stockSearch.symbol
                    
                    if stockSearch.exchDisp == "NASDAQ" || stockSearch.exchDisp == "NYSE"{
                        stock.currency = "USD"
                        stock.exchange = stockSearch.exchDisp
                        
                    }else if stockSearch.exchDisp == "TSE"{
                        stock.currency = "CAD"
                        stock.exchange = "TSE"
                        
                    }else if stockSearch.exchDisp == "CVE"{
                        stock.currency = "CAD"
                        stock.exchange = "CVE"
                        
                    }
                    completionHandler(stock)
                    
                }else{
                    completionHandler(nil)
                    
                }
                
            }else{
                completionHandler(nil)
                
            }
        }
    }
    
    fileprivate func getStockInformation(html: String,name:String) -> Stock? {
        
        let stock = Stock()
        
        if let doc = Kanna.HTML(html: html, encoding: String.Encoding.utf8) {
            
            for show in doc.css("td[data-snapfield='market_cap']"){
                let val = show.parent
                let string = val?.content?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                if string!.characters.count >= 11 {
                    stock.marketCap = String(string!.dropFirst(9))
            
                }
                
            }
            
            for show in doc.css("span[class^='pr']") {
                
                let showString = show.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                stock.price = showString
                break
            }
            
        }
        
        return stock
    }
    
}
